-- 4/30/2018

--Kenneth Willoughby
--Chaticus2
--README

I tried several times to refactor code piecemeal from the internet but continuously
would run up against a wall. Several of us got together to collaborate and I took my
inspiration from James Thomas. He was the most successful in getting this up and running.
I removed excessive and unused code throughout the project. Changed the color scheme
to stand out more. I am going to be incredibly honest. I could not, on my own, figure
out the issues I was up against following the resources I was trying to use. Through 
the group me app that we were using to communicate our findings I found that the repository 
had been updated several times since working on it this afternoon. I know that this readme is 
not what you were expecting and that this app is a complete failure. I have nothing but hope that
my attempt to create at least a portion of the requirements will be enough to see me out of this course
with a C. I am so embarrassed that I am at a place where I am basically begging to pass and I understand
that is not what earns a passing grade. I am not even sure where I stand in this course but I do know that
while I am sub par at this stage, I am beyond intereseted in the subject and this drives me to 
push myself to learn this on my own. I simply do not have the time to close the clear knowledge gap I have.
I sincerely thank you for your instruction this semester and hope that I can come to you one day with
a better understanding of this material. At the time of this writing, the application no longer functions at all,
most likely due to the updates made in the repository not being applied. 
