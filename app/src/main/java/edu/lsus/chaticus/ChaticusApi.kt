package edu.lsus.chaticus

import com.auth0.android.authentication.storage.CredentialsManager
import com.auth0.android.authentication.storage.CredentialsManagerException
import com.auth0.android.callback.BaseCallback
import com.auth0.android.result.Credentials
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Call
import retrofit2.http.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Future
import java.util.concurrent.FutureTask
import java.util.concurrent.TimeUnit
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject

interface ChaticusApi {

    /*@GET("/conversations/{id}/messages")
    @Headers("Auth: Token")
    fun getMessages(@Path("id") conversationId: Int) : Call<List<MessageDto>>*/

    @GET("/conversations/")
    //@Headers("Auth: Token")
    fun getMessages() : Call<List<MessageDto>>

    @POST("/conversations/{id}/messages")
    @Headers("Auth: Token")
    fun postMessage(@Path("id") conversationId: Int) : Call<List<MessageDto>>

    @POST("/conversations/")
    //@Headers("Content-type: application/json")
    fun postConversation(@Body title: RequestBody) : Call<MessageDto>
}

data class MessageDto(
        val title: String)

data class Conversation(
        val conversation: ConversationDto
)

data class ConversationDto(
        val id: Int,
        val title: String
)

class ApiAuthenticationInterceptor(val session: CredentialsManager) : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        var request = chain!!.request()

        val authHeader = request.header("Auth")
        if (authHeader != null) {
            val authFuture = CredentialsFuture(session)
            authFuture.start()
            val credentials = authFuture.get()
            request = request.newBuilder()
                    .header("Authorization", "${credentials.type} ${credentials.accessToken}")
                    .build()
        }

        return chain.proceed(request)
    }

}

class CredentialsFuture(val session: CredentialsManager) : Future<Credentials> {

    private var cancelled = false
    private val countdownLatch = CountDownLatch(1)
    private lateinit var result: Credentials

    override fun cancel(p0: Boolean): Boolean {
        if (isDone) return false

        countdownLatch.countDown()
        cancelled = true
        return !isDone
    }

    override fun isCancelled(): Boolean {
        return cancelled
    }

    override fun isDone(): Boolean {
        return countdownLatch.count == 0L
    }

    override fun get(): Credentials {
        countdownLatch.await()
        return result;
    }

    override fun get(p0: Long, p1: TimeUnit?): Credentials {
        countdownLatch.await(p0, p1)
        return result
    }

    fun start() {
        session.getCredentials(object : BaseCallback<Credentials, CredentialsManagerException> {
            override fun onSuccess(payload: Credentials?) {
                result = payload!!
                countdownLatch.countDown()
            }

            override fun onFailure(error: CredentialsManagerException?) {
                throw error!!
            }
        })
    }
}